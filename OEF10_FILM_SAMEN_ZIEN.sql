USE dbmodernways;
SELECT FILMS.NAAM AS 'film', COUNT(DISTINCT RATINGS.RATING) AS 'Aantal keer beoordeeld', IMDBSCORES.SCORE AS 'IMDB rating'
FROM RATINGS
JOIN FILMS ON RATINGS.FILMS_ID= FILMS.FILM_ID
JOIN PERSONEN ON RATINGS.PERSONEN_ID = PERSONEN.PERSOON_ID
JOIN IMDBSCORES ON RATINGS.FILMS_ID = FILMS.FILM_ID
GROUP BY FILMS.NAAM
ORDER BY RATINGS.RATING ASC, IMDBSCORES.SCORE DESC;