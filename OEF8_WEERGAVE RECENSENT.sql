USE dbmodernways;
SELECT FILMS.NAAM AS 'Film', concat(VOORNAAM," ", FAMILIENAAM) AS 'Recensent', RATINGS.RATING, IMDBSCORES.SCORE AS 'IMDB rating'
FROM PERSONEN
JOIN RATINGS ON PERSONEN.PERSOON_ID=RATINGS.PERSONEN_ID
JOIN FILMS ON RATINGS.FILMS_ID=FILMS.FILM_ID
JOIN IMDBSCORES ON RATINGS.FILMS_ID=IMDBSCORES.FILMS_ID
ORDER BY concat(VOORNAAM," ", FAMILIENAAM);
