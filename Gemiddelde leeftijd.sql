use dbmodernways;
SELECT DISTINCT (FAMILIENAAM) AS 'Familienaam', COUNT(*) AS 'Aantal familieden', AVG(YEAR(CURDATE()) - GEBOORTEJAAR) AS 'Gemiddelede leeftijd'
FROM personen
GROUP BY FAMILIENAAM;